/**
 * Created by mpszonka on 30.10.2019.
 */

trigger DeloitteEvent_trigger on Deloitte_s_Event__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    if (Trigger.isBefore && Trigger.isInsert) {
        DeloitteEvent_triggerHandler.handleBeforeInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isInsert) {
        DeloitteEvent_triggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isBefore && Trigger.isUpdate) {
        DeloitteEvent_triggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isUpdate) {
        DeloitteEvent_triggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isBefore && Trigger.isDelete) {

    }
}