/**
 * Created by mpszonka on 31.10.2019.
 */

public with sharing class DeloitteEvent_fetchCost {

    public static Decimal getEventCost(String eventName) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://deloittelab2019-developer-edition.eu26.force.com/Pricing/services/apexrest/Pricing?externalId=' + eventName);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        if (response.getStatusCode() == 200) {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            if (results != null) {
                return Decimal.valueOf(results.get('product').toString());
            }
        }
        return -1;
    }
}