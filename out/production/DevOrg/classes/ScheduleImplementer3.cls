public with sharing class ScheduleImplementer3 {

    public static void createDeloitteEventOneDayBeforeNotification() {
        DeloitteEventNotifierOneDayBefore d = new DeloitteEventNotifierOneDayBefore();
        String sch = '0 0 10 * * ?';
        String jobID = System.schedule('Deloitte Event-> One Day Before', sch, d);
    }

    public static void createDeloitteEventDailyNotification() {
        DeloitteEventNotifierOneDayBefore d = new DeloitteEventNotifierOneDayBefore();
        String sch = '0 0 10 * * ?';
        String jobID = System.schedule('Deloitte Event-> DailyNotification', sch, d);
    }

    public static void createDeloitteTwoHoursBeforeNotification() {
        DeloitteEventNotifierOneDayBefore d = new DeloitteEventNotifierOneDayBefore();
        String sch = '0 0 * * * ?';
        String jobID = System.schedule('Deloitte Event-> TwoHoursBeforeNotification', sch, d);
    }

    public static void createDeloitteEventHourlyNotification() {
        DeloitteEventNotifierOneDayBefore d = new DeloitteEventNotifierOneDayBefore();
        String sch = '0 0 * * * ?';
        String jobID = System.schedule('Deloitte Event-> HourlyNotification', sch, d);
    }
}