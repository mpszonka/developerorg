public with sharing class DeloitteEvent_notifierEmployees implements Schedulable {
    public void execute(SchedulableContext param1) {
        List<Deloitte_s_Event__c> deloitteEvents = [SELECT name, Start_Date__c, (SELECT name, User__c FROM event_members__r WHERE User__c != null) FROM Deloitte_s_Event__c];

        for (Deloitte_s_Event__c deloitteEvent : deloitteEvents) {
            if (deloitteEvent.Start_Date__c != null && deloitteEvent.Start_Date__c < Datetime.now().addHours(2)) {
                for (Event_Member__c eventMember : deloitteEvent.Event_Members__r) {
                    ChatterPoster.post(eventMember.User__c, 'Remeber, today ' + deloitteEvent.Name + ' starts at: ' + deloitteEvent.Start_Date__c.hour() + ':' + deloitteEvent.Start_Date__c.minute());
                    System.debug('posted to: ' + eventMember.Name);
                }
            }
        }
    }
}