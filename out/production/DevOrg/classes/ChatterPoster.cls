/**
 * Created by mpszonka on 29.10.2019.
 */

public with sharing class ChatterPoster {
    public static void post(Id userID, string body) {
        FeedItem post = new FeedItem();
        post.ParentId = userID;
        post.Body = body;
        insert post;
    }
}