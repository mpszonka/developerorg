public with sharing class DeloitteEventNotifierOneDayBefore implements Schedulable {
    public void execute(SchedulableContext param1) {
        ChatterPoster.post(Id.valueOf('0053X00000AV3b8QAD'), 'DeloitteEventNotifierOneDayBefore ');
    }
}