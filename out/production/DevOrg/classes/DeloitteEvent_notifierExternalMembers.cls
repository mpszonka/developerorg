public with sharing class DeloitteEvent_notifierExternalMembers implements Schedulable {
    public void execute(SchedulableContext param1) {
        List<Deloitte_s_Event__c> deloitteEvents = [SELECT name, Start_Date__c, (SELECT name, Contact__c, email__c FROM event_members__r WHERE Contact__c != null) FROM Deloitte_s_Event__c];
        EmailTemplate emailTemplateId = [SELECT Id FROM EmailTemplate WHERE Name = 'DeloitteEventNotificationOneDayBefore' LIMIT 1];
        List<EmailReceiver> emailReceivers = new List<EmailReceiver>();
        for (Deloitte_s_Event__c deloitteEvent : deloitteEvents) {
            if (deloitteEvent.Start_Date__c != null && deloitteEvent.Start_Date__c.isSameDay(Date.today().addDays(1))) {
                for (Event_Member__c eventMember : deloitteEvent.Event_Members__r) {
                    if (eventMember.email__c != null) {
                        emailReceivers.add(new EmailReceiver(eventMember.email__c, eventMember.Contact__c, eventMember.Id));
                    }
                }
                EmailManager emailManager = new EmailManager(emailReceivers, emailTemplateId.Id, null);
                emailManager.sendEmails();
            }
        }
    }
}