/**
 * Created by mpszonka on 30.10.2019.
 */
public with sharing class EventMember_triggerHandler {

    public static void handleBeforeInsert(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        EventMember_utilities.preventContactAssignmentIfEventIsNotExternal(newList, oldMap);
        EventMember_utilities.preventBothContactAndUserAssignment(newList);
        EventMember_utilities.preventNoCommentIfRejected(newList);
        EventMember_utilities.preventDuplication(newList, oldMap);
    }

    public static void handleAfterInsert(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);
    }

    public static void handleBeforeUpdate(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        EventMember_utilities.preventContactAssignmentIfEventIsNotExternal(newList, oldMap);
        EventMember_utilities.preventBothContactAndUserAssignment(newList);
        EventMember_utilities.preventNoCommentIfRejected(newList);
        EventMember_utilities.preventDuplication(newList, oldMap);
    }

    public static void handleAfterUpdate(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);

    }

    public static void handleAfterDelete(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);
    }
}