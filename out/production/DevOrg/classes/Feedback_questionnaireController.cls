/**
 * Created by mpszonka on 07.11.2019.
 */

public with sharing class Feedback_questionnaireController {
    @AuraEnabled(Cacheable=true)
    public static List<Feedback__c> getUserFeedback() {
        return [SELECT Id, Attended__c, Comment__c, MemberAge__c, Score__c, NotRecommendReason__c, If_recommend__c FROM Feedback__c LIMIT 1];
    }
}