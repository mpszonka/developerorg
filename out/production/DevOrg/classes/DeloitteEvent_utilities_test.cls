@isTest
public with sharing class DeloitteEvent_utilities_test {

//    @isTest
//    public static void shouldApproveIfCostLowerThan100AfterInsert() {
//        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
//        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', Cost__c = 10, ExternalId__c = 'Product_500'));
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent2', Cost__c = 99, ExternalId__c = 'Product_500'));
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent3', Cost__c = 100, ExternalId__c = 'Product_500'));
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent4', Cost__c = 101, ExternalId__c = 'Product_500'));
//
//        Test.startTest();
//        insert testEvents;
//        Test.stopTest();
//
//        List<Deloitte_s_Event__c> insertedEvents = [SELECT Name, Approval_Status__c FROM Deloitte_s_Event__c ORDER BY Name];
//        System.assertEquals(insertedEvents[0].Approval_Status__c, 'Approved');
//        System.debug(insertedEvents[1].Approval_Status__c + ' ' +  insertedEvents[1].Name);
//        System.assertEquals(insertedEvents[1].Approval_Status__c, 'Approved');
//        System.assertEquals(insertedEvents[2].Approval_Status__c, null);
//        System.assertEquals(insertedEvents[3].Approval_Status__c, null);
//    }

//    @IsTest
//    public static void shouldApproveIfCostLowerThan100AfterUpdate() {
//        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
//        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', Cost__c = 99, ExternalId__c = 'Product_500'));
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent2', Cost__c = 99, ExternalId__c = 'Product_500'));
//        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent3', Cost__c = 100, ExternalId__c = 'Product_500'));
//        insert testEvents;
//
//        Test.startTest();
//        testEvents = new List<Deloitte_s_Event__c>([SELECT Name, Approval_Status__c, Cost__c FROM Deloitte_s_Event__c ORDER BY Name ASC]);
//        testEvents[0].Cost__c = 80;
//        testEvents[1].Cost__c = 100;
//        testEvents[2].Cost__c = 101;
//        update testEvents;
//        Test.stopTest();
//
//        List<Deloitte_s_Event__c> updatedEvents = new List<Deloitte_s_Event__c>([SELECT Name, Approval_Status__c, Cost__c FROM Deloitte_s_Event__c ORDER BY Name ASC]);
//        System.assertEquals(updatedEvents[0].Approval_Status__c, 'Approved');
//        System.assertEquals(updatedEvents[1].Approval_Status__c, 'Approved');
//        System.assertEquals(updatedEvents[2].Approval_Status__c, null);
//    }

    @IsTest
    public static void shouldSetStatusCancelledIfRejectedBeforeUpdate() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', Cost__c = 99, ExternalId__c = 'Product_500', Approval_Status__c = 'Approved'));
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent2', Cost__c = 99, ExternalId__c = 'Product_500'));
        insert testEvents;
        testEvents = new List<Deloitte_s_Event__c>([SELECT Approval_Status__c FROM Deloitte_s_Event__c ORDER BY Name ASC]);
        testEvents[0].Approval_Status__c = 'Rejected';
        testEvents[1].Approval_Status__c = 'Pending';

        Test.startTest();
        update testEvents;
        Test.stopTest();

        List<Deloitte_s_Event__c> updatedTestEvents = new List<Deloitte_s_Event__c>([SELECT Approval_Status__c, Status__c FROM Deloitte_s_Event__c ORDER BY Name ASC]);
        System.assertNotEquals(updatedTestEvents[0].Approval_Status__c, testEvents[0].Approval_Status__c);
        System.assertNotEquals(updatedTestEvents[1].Approval_Status__c, testEvents[1].Approval_Status__c);
        System.assertEquals(updatedTestEvents[0].Status__c, 'Cancelled');
        System.assertNotEquals(updatedTestEvents[1].Status__c, 'Cancelled');

    }

    @IsTest
    public static void shouldCalcNumberOfMembersAfterInsert() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', ExternalId__c = 'Product_500'));
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent2', ExternalId__c = 'Product_500'));
        insert testEvents;

        testEvents = [SELECT Id FROM Deloitte_s_Event__c ORDER BY Name];

        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Deloitte_Event__c = testEvents[0].Id));
        testEventMembers.add(new Event_Member__c(Name = 'eventMember2', Deloitte_Event__c = testEvents[1].Id));

        Test.startTest();
        insert testEventMembers;
        Test.stopTest();

        testEvents = [SELECT Id, Number_of_Participants__c FROM Deloitte_s_Event__c ORDER BY Name];
        System.assertEquals(testEvents[0].Number_of_Participants__c, 1);
    }

    @IsTest
    public static void shouldCalcNumberOfMembersAfterUpdate() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', ExternalId__c = 'Product_500'));
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent2', ExternalId__c = 'Product_500'));
        insert testEvents;

        testEvents = [SELECT Id FROM Deloitte_s_Event__c ORDER BY Name];
        String testEvent1Id = testEvents[0].Id;
        String testEvent2Id = testEvents[1].Id;
        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Deloitte_Event__c = testEvent1Id));
        testEventMembers.add(new Event_Member__c(Name = 'eventMember2', Deloitte_Event__c = testEvent2Id));

        insert testEventMembers;

        Test.startTest();
        testEventMembers = [SELECT Deloitte_Event__c FROM Event_Member__c ORDER BY Name];
        testEventMembers[0].Deloitte_Event__c = testEvent2Id;
        update testEventMembers;
        Test.stopTest();

        testEvents = [SELECT Id, Number_of_Participants__c FROM Deloitte_s_Event__c ORDER BY Name];
        System.assertEquals(testEvents[0].Number_of_Participants__c, 0);
        System.assertEquals(testEvents[1].Number_of_Participants__c, 2);
    }

}