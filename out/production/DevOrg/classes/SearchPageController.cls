/**
 * Created by mpszonka on 08.11.2019.
 */

public with sharing class SearchPageController {
    @AuraEnabled(Cacheable=true)
    public static List<Account> getEmployersList() {
        return [SELECT Name FROM Account];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Account> getEmployersJobOffers(Id employerId) {
        return [SELECT (SELECT Name FROM JobOffers__r) FROM Account where Name = :employerId];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Skill__c> getSkills() {
        return [SELECT Name FROM Skill__c];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Candidate__c> getCandidatesBasedOnSkills(List<String> skillsIds) {
        List<Candidate__c> candidates = [SELECT Name, (SELECT name, SkillLevel__c, Skill__c FROM CandidateSkills__r) FROM Candidate__c];
        List<Candidate__c> filteredCandidates = new List<Candidate__c>();
        for (Candidate__c candidate :  candidates) {
            for (CandidateSkill__c candidateSkill : candidate.CandidateSkills__r) {
                if (skillsIds.contains(candidateSkill.Skill__c) && !filteredCandidates.contains(candidate)) {
                    filteredCandidates.add(candidate);
                    System.debug('candidate ' + candidate.Name + 'added');
                }
            }
        }
        return filteredCandidates;
    }
}