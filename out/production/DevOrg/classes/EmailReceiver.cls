/**
 * Created by mpszonka on 04.11.2019.
 */

public with sharing class EmailReceiver {
    public String emailAddress;
    public Id targetObjectId;
    public Id whatId;

    public EmailReceiver(String emailAddress, Id targetObjectId, Id whatId) {
        this.emailAddress = emailAddress;
        this.targetObjectId = targetObjectId;
        this.whatId = whatId;
    }
}