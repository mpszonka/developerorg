/**
 * Created by mpszonka on 06.11.2019.
 */

({
    showErrorToast: function(errorMessage) {
        toastEvent.setParams({
            "title": "Success!",
            "type": "error",
            "message": errorMessage,
        });
        toastEvent.fire();
    },

    showSuccessToast: function(message) {
        const toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": message,
        });
        toastEvent.fire();
    },
});