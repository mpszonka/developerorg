import { LightningElement, api, track, wire } from 'lwc';
import getEmployers from '@salesforce/apex/SearchPageController.getEmployersList';
import getSkills from '@salesforce/apex/SearchPageController.getSkills';

export default class SearchPage extends LightningElement {

   @track searchByJobOffers = 'searchByJobOffers';
   employer = '';
   skills = [];
   @track employersComboBoxValues;
   @track skillsComboBoxValues;

   @wire(getEmployers)
   wiredEmployersList({ error, data }) {
       if (data) {
           this.setEmployersListComboBoxValues(data);
           console.log('data', data);
       } else if (error) {
           console.log('error', error);
       }
   }

   @wire(getSkills)
   wiredSkillsComboBoxValues({ error, data }) {
       if (data) {
           this.setSkillsListComboBoxValues(data);
           console.log('data', data);
       } else if (error) {
           console.log('error', error);
       }
   }

   setEmployersListComboBoxValues(data) {
        this.employersComboBoxValues = data.map((element) => ({
            label: element.Name,
            value: element.Id
        }));
        console.log('this.employersComboBoxValues', this.employersComboBoxValues);
   }


   setSkillsListComboBoxValues(data) {
        this.skillsComboBoxValues = data.map((element) => ({
            label: element.Name,
            value: element.Id
        }));
        console.log('this.skillsComboboxValues', this.skillsComboBoxValues);
   }

   handleChange(event) {
       this[event.target.name] = event.detail.value;
       console.log('this[event.target.name] ',event.target.name,  this[event.target.name]);
   }

   handleSubmit(event) {
       event.preventDefault();
       console.log('handle submit');
   }
}