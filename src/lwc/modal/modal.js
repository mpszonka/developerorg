/**
 * Created by mpszonka on 11.11.2019.
 */

import { LightningElement } from 'lwc';

export default class Modal extends LightningElement {

   @track openModal = false;

   openmodal() {
       this.openmodel = true
   }

   closeModal() {
       this.openmodel = false
   }

}