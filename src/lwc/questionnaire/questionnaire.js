import { LightningElement, api, track, wire } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import FEEDBACK_OBJECT from '@salesforce/schema/Feedback__c';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';
import getUserFeedback from '@salesforce/apex/Feedback_questionnaireController.getUserFeedback';


export default class DemoRelatedContactsList extends LightningElement {

    @api recordId;

    @track userName;
    @track score = 0;
    @track ifRecommend = true;

    @wire(getRecord, {recordId: USER_ID, fields: [NAME_FIELD]})
    user({ error, data }) {
        if (error) {
            this.error = error ;
        } else if (data) {
            this.userName = data.fields.Name.value;
        }
    };

    @wire(getUserFeedback)
    wiredFeedback({ error, data }) {
                     if (data) {
                         this.feedback =  JSON.stringify(data);
                         console.log('feedback', this.feedback);
                     } else if (error) {
                         this.initNewFeedback();
                     }
                 }

    scoringOptions = new Array(10).fill({}).map((element, key) => ({value: key+1, label: key+1}));

    initNewFeedback() {
        this.feedback = {
           Attended__c: true,
           If_recommend__c: true,
        };
    }

    handleInputChange(event) {
        console.log('this.das', this.feedback);
//        if (event.target.type === 'checkbox') {
//            if (event.target.name === 'If_recommend__c') {
//                this.ifRecommend = event.target.checked;
//            }
//            this.feedback[event.target.name] = event.target.checked;
//        } else {
//            this.feedback[event.target.name] = event.target.value;
//        }
    }

    handleScoreInput(event) {
        this.feedback.Score__c = event.target.value;
        this.score = event.target.value;
    }

    setAdditionalRecordFields() {
        this.feedback.Deloitte_s_Event__c = this.recordId;
        this.feedback.User__c = USER_ID;
    }

    handleSubmit(event) {
        event.preventDefault();
        this.createFeedback();
    }

    createFeedback() {
        this.setAdditionalRecordFields();
        const recordInput = { apiName: FEEDBACK_OBJECT.objectApiName, fields: this.feedback };
        createRecord(recordInput)
            .then(feedback => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Feedback saved',
                        variant: 'success',
                    }),
                );
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error saving feedback',
                        message: error.body.message,
                        variant: 'error',
                    }),
                );
            });
    }
}