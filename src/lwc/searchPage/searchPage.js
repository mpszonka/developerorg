import { LightningElement, api, track, wire } from 'lwc';
import getEmployers from '@salesforce/apex/SearchPageController.getEmployersList';
import getSkills from '@salesforce/apex/SearchPageController.getSkills';
import getCandidatesBasedOnSkillsAndEmployerTier from '@salesforce/apex/SearchPageController.getCandidatesBasedOnSkillsAndEmployerTier';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

    const actions = [
        { label: 'Share with employer',
          name: 'shareWithEmployer' }
    ];

    const columns = [
        { label: 'Candidate name', fieldName: 'name' },
        { label: 'Email', fieldName: 'email', type: 'email' },
        { label: 'Skills', fieldName: 'skills'},
        {
            type: 'action',
            typeAttributes: { rowActions: actions },
        },
    ];

export default class SearchPage extends LightningElement {

   employer;
   skills = [];
   @track employersComboBoxValues;
   @track skillsComboBoxValues;
   @track candidates;
   @track columns = columns;
   @track submitButtonDisabled = true;
   @track modalOpened = false;

   @wire(getEmployers)
   wiredEmployersList({ error, data }) {
       if (data) {
           this.setEmployersListComboBoxValues(data);
           console.log('data', data);
       } else if (error) {
           console.log('error', error);
       }
   }

   @wire(getSkills)
   wiredSkillsComboBoxValues({ error, data }) {
       if (data) {
           this.setSkillsListComboBoxValues(data);
           console.log('data', data);
       } else if (error) {
           console.log('error', error);
       }
   }

   openModal() {
       this.modalOpened = true
   }

   closeModal() {
       this.modalOpened = false
   }

   showToast() {
       const showSuccess = new ShowToastEvent({
           title: 'Success!!',
           message: 'This is Success message.',
           variant: 'Success',
       });
       this.dispatchEvent(showSuccess);
    }

   handleRowAction(event) {
        const actionName = event.detail.action.name;
        console.log('actionName', actionName);
        const row = event.detail.row;
        switch (actionName) {
            case 'shareWithEmployer':
                this.showToast();
                break;
        }
   }

   shareCandidateWithEmployer() {
       console.log('sharing row', row);
   }

   setEmployersListComboBoxValues(data) {
        this.employersComboBoxValues = data.map((element) => ({
            label: element.Name,
            value: element.Id
        }));
        console.log('this.employersComboBoxValues', this.employersComboBoxValues);
   }

   setSkillsListComboBoxValues(data) {
        this.skillsComboBoxValues = data.map((element) => ({
            label: element.Name,
            value: element.Id
        }));
        console.log('this.skillsComboboxValues', this.skillsComboBoxValues);
   }

   setCandidatesDataTableValues(data){
       this.candidates = [];
       for(let i = 0; i < data.length; i++) {
           let candidate = {};
           candidate.name = data[i].Name;
           candidate.email = data[i].Email__c;
           if (data[i].CandidateSkills__r) {
               let skills = '';
               data[i].CandidateSkills__r.forEach(skill => skills += ', ' + skill.Skill__r.Name + ' ' + skill.SkillLevel__c);
               candidate.skills = skills;
           }
           this.candidates.push(candidate);
       }
   }

   handleChange(event) {
       this[event.target.name] = event.detail.value;
       console.log('this[event.target.name] ',event.target.name,  this[event.target.name]);
   }

   handleSubmit(event) {
       event.preventDefault();
       console.log('handle submit');
       console.log('this.skills, this.employer', this.skills, this.employer);
       getCandidatesBasedOnSkillsAndEmployerTier({skillsIds: this.skills, employerId: this.employer})
        .then( data => {
            this.setCandidatesDataTableValues(data);
            console.log(' this.candidates',  this.candidates);
        })
        .catch( error => {
            console.log('error', error);
            this.candidates = [];
        })
   }
}