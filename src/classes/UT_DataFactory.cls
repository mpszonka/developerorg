/**
 * Created by mpszonka on 04.11.2019.
 */
@IsTest
public with sharing class UT_DataFactory {
    private static Profile profile {
        get {
            if (profile == null) {
                profile = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
            }
            return profile;
        }
        set;
    }

    public static User getTestUser() {
        return new User(LastName = 'test user last name 1 ',
                FirstName = 'test user first name 1 ',
                Alias = 'jliv 1 ',
                Email = 'jason.liveston@asdf.com',
                Username = 'testUserUsername@test.pl' + System.now(),
                ProfileId = UT_DataFactory.profile.Id,
                TimeZoneSidKey = 'GMT',
                LanguageLocaleKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LocaleSidKey = 'en_US'
        );
    }
}