/**
 * Created by mpszonka on 04.11.2019.
 */
@IsTest
public with sharing class EventMember_utilities_test {

    @IsTest
    public static void shouldPreventAssignContactIfDeloitteEventIsNotExternal() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', ExternalId__c = 'Product_500', Type__c = 'Internal'));
        insert testEvents;

        List<Contact> testContacts = new List<Contact>();
        testContacts.add(new Contact(LastName = 'contact1'));
        insert testContacts;

        testEvents = [SELECT Id FROM Deloitte_s_Event__c ORDER BY Name];
        testContacts = [SELECT Id FROM Contact];

        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Deloitte_Event__c = testEvents[0].Id, Contact__c = testContacts[0].Id));

        Boolean insertPrevented = false;
        Test.startTest();
        try {
            insert testEventMembers;
            Test.stopTest();
        } catch (Exception e) {
            System.debug(e.getMessage());
            System.assert(e.getMessage().contains('Cannot add account to not external event'));
            insertPrevented = true;
        }
        System.assertEquals(insertPrevented, true);
    }

    @IsTest
    public static void shouldAllowAssignContactIfDeloitteEventIsExternal() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        List<Deloitte_s_Event__c> testEvents = new List<Deloitte_s_Event__c>();
        testEvents.add(new Deloitte_s_Event__c(Name = 'deloitteEvent1', ExternalId__c = 'Product_500', Type__c = 'External'));
        insert testEvents;

        List<Contact> testContacts = new List<Contact>();
        testContacts.add(new Contact(LastName = 'contact1'));
        insert testContacts;

        testEvents = [SELECT Id FROM Deloitte_s_Event__c ORDER BY Name];
        testContacts = [SELECT Id FROM Contact];

        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Deloitte_Event__c = testEvents[0].Id, Contact__c = testContacts[0].Id));

        Test.startTest();
        insert testEventMembers;
        Test.stopTest();

        testEventMembers = [SELECT Id, Deloitte_Event__c FROM Event_Member__c];
        System.assertEquals(testEventMembers[0].Deloitte_Event__c, testEvents[0].Id);
    }

    @IsTest
    public static void shouldPreventAssignBothUserAndContactAfterInsert() {
        Contact testContact = new Contact(LastName = 'contact1');
        insert testContact;
        User testUser = UT_DataFactory.getTestUser();
        insert testUser;


        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Contact__c = testContact.Id, User__c = testUser.Id));
        testEventMembers.add(new Event_Member__c(Name = 'eventMember2', Contact__c = testContact.Id));
        testEventMembers.add(new Event_Member__c(Name = 'eventMember3', User__c = testUser.Id));

        Test.startTest();
        List<Database.SaveResult> saveResults = Database.insert(testEventMembers, false);
        Test.stopTest();

        System.assert(saveResults[0].getErrors()[0].getMessage().contains('Cannot add contact and user, choose one'));
        System.assertNotEquals(null, saveResults[1].id);
        System.assertNotEquals(null, saveResults[2].id);
    }

//    @IsTest
//    public static void shouldPreventAssignBothUserAndContactAfterUpdate() {
//        List<Contact> testContacts = new List<Contact>();
//        testContacts.add(new Contact(LastName = 'contact1'));
//        insert testContacts;
//        List<User> usersList = new List<User>();
//        usersList.add(UT_DataFactory.getTestUser());
//        insert usersList;
//
//        testContacts = [SELECT Id FROM Contact];
//        List<User> testUsers = [SELECT Id FROM User];
//
//        List<Event_Member__c> testEventMembers = new List<Event_Member__c>();
//        testEventMembers.add(new Event_Member__c(Name = 'eventMember1', Contact__c = testContacts[0].Id));
//        testEventMembers.add(new Event_Member__c(Name = 'eventMember2', Contact__c = testContacts[0].Id));
//        testEventMembers.add(new Event_Member__c(Name = 'eventMember3', User__c = testUsers[0].Id));
//        testEventMembers.add(new Event_Member__c(Name = 'eventMember4', User__c = testUsers[0].Id));
//        insert testEventMembers;
//
//        testEventMembers = [SELECT Name, Contact__c, User__c FROM Event_Member__c ORDER BY Name];
//
//        testEventMembers[0].User__c = testUsers[0].Id;
//
//        testEventMembers[1].User__c = testUsers[1].Id;
//        testEventMembers[1].Contact__c = null;
//
//        testEventMembers[2].User__c = testUsers[0].Id;
//        testEventMembers[2].User__c = testUsers[0].Id;
//
//        Test.startTest();
//        update testEventMembers;
//        Test.stopTest();
//
//        for (Database.SaveResult sr : saveResults) {
//            if (!sr.isSuccess()) {
//                System.assert(sr.getErrors()[0].getMessage().contains('Cannot add contact and user, choose one'));
//            } else {
//                System.assertNotEquals(null, sr.id);
//            }
//        }
//    }
}