/**
 * Created by mpszonka on 10.11.2019.
 */

public with sharing class Assignement_triggerHandler {
    public static void handleBeforeInsert(List<Assignement__c> newList,  Map<Id, Assignement__c> oldMap) {
        Assignement_utils.setWeeklyCost(newList, oldMap);
    }

    public static void handleAfterInsert(List<Assignement__c> newList,  Map<Id, Assignement__c> oldMap){

    }

    public static void handleBeforeUpdate(List<Assignement__c> newList, Map<Id, Assignement__c> oldMap) {
        Assignement_utils.setWeeklyCost(newList, oldMap);
    }

    public static void handleAfterUpdate(List<Assignement__c> newList,  Map<Id, Assignement__c> oldMap){

    }
}