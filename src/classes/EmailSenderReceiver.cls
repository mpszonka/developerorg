/**
 * Created by mpszonka on 04.11.2019.
 */

public with sharing class EmailSenderReceiver {
    public Contact contact;
    public String emailAddress;
}