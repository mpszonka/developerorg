/**
 * Created by mpszonka on 07.11.2019.
 */

public with sharing class DeloitteEventController_Scoring {
    @AuraEnabled
    public static String getVoteId(String userId, String deloitteEventId) {
        userVote__c vote = [SELECT Id FROM userVote__c WHERE Deloitte_s_Event__c=:deloitteEventId AND User__c=:userId];
        return (String) vote.Id;
    }
}