/**
 * Created by mpszonka on 11.11.2019.
 */

public with sharing class TeamMember_experienceCalc implements Schedulable  {
    public void execute(SchedulableContext param1) {

    }

    public void calcExperience() {
        Map<Id, Team_member__c> teamMembers = new Map<Id, Team_member__c>([SELECT Id, (SELECT Id, Skill2__c FROM Experiences) FROM Team_member__c]);
        List<Assignement_Skill__c> assignementSkills = [SELECT Skill2__c, Assignement__c, Assignement__r.Team_member__c FROM Assignement_Skill__c];
        for (Assignement_Skill__c assignementSkill : assignementSkills) {

        }
    }
}