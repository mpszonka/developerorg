/**
 * Created by mpszonka on 08.11.2019.
 */

public with sharing class SearchPageController {
    @AuraEnabled(Cacheable=true)
    public static List<Account> getEmployersList() {
        return [SELECT Name FROM Account];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Account> getEmployersJobOffers(Id employerId) {
        return [SELECT (SELECT Name FROM JobOffers__r) FROM Account where Name = :employerId];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Skill__c> getSkills() {
        return [SELECT Name FROM Skill__c];
    }

    @AuraEnabled(Cacheable=true)
    public static List<Candidate__c> getCandidatesBasedOnSkillsAndEmployerTier(List<String> skillsIds, String employerId) {
        Account employer = [SELECT Tier__c, Name FROM Account WHERE Id=:employerId];
        List<Candidate__c> candidates = [SELECT Name, Tier__c, Email__c, (SELECT SkillLevel__c, Skill__r.Name FROM CandidateSkills__r) FROM Candidate__c];
        List<Candidate__c> filteredCandidates = new List<Candidate__c>();
        for (Candidate__c candidate :  candidates) {
            for (CandidateSkill__c candidateSkill : candidate.CandidateSkills__r) {
                if (skillsIds.contains(candidateSkill.Skill__c) && !filteredCandidates.contains(candidate) && candidate.Tier__c.contains(employer.Tier__c)) {
                    filteredCandidates.add(candidate);
                    System.debug('candidate ' + candidate.Name + 'added');
                }
            }
        }
        return filteredCandidates;
    }
}