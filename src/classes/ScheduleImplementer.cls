/**
 * Created by mpszonka on 02.11.2019.
 */

public with sharing class ScheduleImplementer {

    public static void createDeloitteEventExternalMembersNotification() {
        DeloitteEvent_notifierExternalMembers notifier = new DeloitteEvent_notifierExternalMembers();
        String sch = '0 0 9 1/1 * ? *';
        System.schedule('Deloitte Event-> OneDayBeforeExternalMembersNotification', sch, notifier);
    }

    public static void createDeloitteEventTwoHoursBeforeNotification() {
        DeloitteEvent_notifierEmployees notifier = new DeloitteEvent_notifierEmployees();
        String sch = '0 0 0/2 1/1 * ? *';
        System.schedule('Deloitte Event-> TwoHoursBeforeInternalUsersNotification', sch, notifier);
    }
}