global class EmailSender {
    public List<EmailReceiver> receivers;
    public String emailTemplateId;
    public String whatId;
    public String emailBody;

    public EmailSender(List<EmailReceiver> receivers, String emailTemplateId, String whatId, String emailBody) {
        this.receivers = receivers;
        this.emailTemplateId = emailTemplateId;
        this.whatId = whatId;
        this.emailBody = emailBody;
    }

    public void sendEmails() {
        for (EmailReceiver receiver : this.receivers) {
            this.sendEmail(receiver.emailAddress, receiver.contact.Id);
        }
    }

    private void sendEmail(String receiverAddress, String targetObjectId) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new List<String>{
                receiverAddress
        };
        message.setSenderDisplayName('Deloitte Support');
        message.setUseSignature(false);
        message.setBccSender(false);
        if (emailTemplateId != null) {
            message.setTemplateId(this.emailTemplateId);
            message.setTargetObjectId(targetObjectId);
            //message.plainTextBody = emailTemplate.Body;
            message.setWhatId(this.whatID);
        } else {
            message.plainTextBody = this.emailBody;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                message
        };
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email has been sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
}