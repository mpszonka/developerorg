/**
 * Created by mpszonka on 30.10.2019.
 */
public with sharing class EventMemberHandler2 {

    public static void handleBeforeInsert(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);
    }

    public static void handleAfterInsert(Map<Id, Case> newMap) {

    }

    public static void handleBeforeUpdate(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);
        EventMember_utilities.preventContactAssignmentIfEventIsNotExternal(newList);
        EventMember_utilities.preventBothContactAndUserAssignment(newList);
        EventMember_utilities.prevenyNoCommentIfRejected(newList);
    }

    public static void handleAfterUpdate(Map<Id, Case> newMap, Map<Id, Case> oldMap) {

    }

    public static void handleAfterDelete(List<Event_Member__c> newList, Map<Id, Event_Member__c> oldMap) {
        DeloitteEvent_utilities.calcNumberOfParticipants(newList, oldMap);
    }
}