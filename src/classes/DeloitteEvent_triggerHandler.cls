/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class DeloitteEvent_triggerHandler {

    public static void handleBeforeInsert(List<Deloitte_s_Event__c> newList,  Map<Id, Deloitte_s_Event__c> oldMap) {
        DeloitteEvent_utilities.automaticallyApproveBasedOnCost(newList, oldMap);
        DeloitteEvent_utilities.setCancelStatusIfRejected(newList, oldMap);
        DeloitteEvent_utilities.preventMoreParticipantsThanSeats(newList, oldMap);
        DeloitteEvent_utilities.calcPricePerParticipant(newList, oldMap);
        DeloitteEvent_utilities.preventPendingApprovalStatus(newList, oldMap);
    }

    public static void handleAfterInsert(List<Deloitte_s_Event__c> newList,  Map<Id, Deloitte_s_Event__c> oldMap){
        DeloitteEvent_utilities.setCostBasedOnExternalId(newList, oldMap);
    }

    public static void handleBeforeUpdate(List<Deloitte_s_Event__c> newList, Map<Id, Deloitte_s_Event__c> oldMap) {
        DeloitteEvent_utilities.automaticallyApproveBasedOnCost(newList, oldMap);
        DeloitteEvent_utilities.setCancelStatusIfRejected(newList, oldMap);
        DeloitteEvent_utilities.preventMoreParticipantsThanSeats(newList, oldMap);
        DeloitteEvent_utilities.calcPricePerParticipant(newList, oldMap);
        DeloitteEvent_utilities.preventPendingApprovalStatus(newList, oldMap);
    }

    public static void handleAfterUpdate(List<Deloitte_s_Event__c> newList,  Map<Id, Deloitte_s_Event__c> oldMap){
        DeloitteEvent_utilities.setCostBasedOnExternalId(newList, oldMap);
    }
}