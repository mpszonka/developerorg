/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class EventMember_utilities {
    private static Boolean contactFieldChanged(Event_Member__c newEventMember, Event_Member__c oldEventMember) {
        return newEventMember.Contact__r != oldEventMember.Contact__r;
    }

    private static Boolean userFieldChanged(Event_Member__c newEventMember, Event_Member__c oldEventMember) {
        return newEventMember.User__c != oldEventMember.User__c;
    }

    public static void preventContactAssignmentIfEventIsNotExternal(List<Event_Member__c> newEventMembers, Map<Id, Event_Member__c> oldMap) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (!Trigger.isInsert && !EventMember_utilities.contactFieldChanged(newEventMember, oldMap.get(newEventMember.Id))) {
                continue;
            }
            if (newEventMember.Deloitte_Event__c == null) {
                continue;
            }
            Id newRelatedEventId = newEventMember.Deloitte_Event__c;
            List<Deloitte_s_Event__c> deloitteSEvents = [select Number_of_Participants__c, Type__c from Deloitte_s_Event__c where id = :newRelatedEventId];
            Deloitte_s_Event__c deloitteSEvent = deloitteSEvents[0];

            if (newEventMember.Contact__c != null && deloitteSEvent.Type__c != 'External') {
                newEventMember.addError('Cannot add account to not external event');
            }
        }
    }

    public static void preventBothContactAndUserAssignment(List<Event_Member__c> newEventMembers) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Contact__c != null && newEventMember.User__c != null) {
                newEventMember.addError('Cannot add contact and user, choose one');
            }
        }
    }

    public static void preventNoCommentIfRejected(List<Event_Member__c> newEventMembers) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Status__c == 'Rejected' && newEventMember.Comments__c == null) {
                newEventMember.addError('If status is rejected, comments are needed');
            }
        }
    }


    //prevent two eventMembers with the same user or contact assigned to the same DeloitteEvent
    public static void preventDuplication(List<Event_Member__c> newEventMembers, Map<Id, Event_Member__c> oldMap) {
        Set<Id> eventsIds = new Set<Id>();
        for (Event_Member__c newEventMember : newEventMembers) {
            eventsIds.add(newEventMember.Deloitte_Event__c);
        }
        if (!Trigger.isInsert) {
            for (Event_Member__c oldEventMember : oldMap.values()) {
                eventsIds.add(oldEventMember.Deloitte_Event__c);
            }
        }

        Map<Id, Deloitte_s_Event__c> deloitteSEvents = new Map<Id, Deloitte_s_Event__c>([SELECT Id, (SELECT User__c, Contact__c FROM Event_Members__r) FROM Deloitte_s_Event__c WHERE Id IN :eventsIds]);

        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Deloitte_Event__c == null) {
                continue;
            }
            if  (!Trigger.isInsert
                    && !EventMember_utilities.contactFieldChanged(newEventMember, oldMap.get(newEventMember.Id))
                    && !EventMember_utilities.userFieldChanged(newEventMember, oldMap.get(newEventMember.Id))) {
                continue;
            }

            Id newEventMemberUserId = newEventMember.User__c;
            Id newEventMemberContactId = newEventMember.Contact__c;
            List<Event_Member__c> currentEventMembers = deloitteSEvents.get(newEventMember.Deloitte_Event__c).Event_Members__r;
            for (Event_Member__c eventMember : currentEventMembers) {
                if (eventMember.User__c != null && eventMember.User__c == newEventMemberUserId) {
                    newEventMember.addError('this user is already assigned to the event');
                    break;
                }
                if (eventMember.Contact__c != null && eventMember.Contact__c == newEventMemberContactId) {
                    newEventMember.addError('this contact is already assigned to the event');
                    break;
                }
            }
        }
    }
}