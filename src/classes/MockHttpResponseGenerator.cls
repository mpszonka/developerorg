/**
 * Created by mpszonka on 04.11.2019.
 */

@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req) {
        //System.assertEquals('https://deloittelab2019-developer-edition.eu26.force.com/Pricing/services/apexrest/Pricing?externalId=Product_500', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"product":"20"}');
        res.setStatusCode(200);
        return res;
    }
}