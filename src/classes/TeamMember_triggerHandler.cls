/**
 * Created by mpszonka on 10.11.2019.
 */

public with sharing class TeamMember_triggerHandler {
    public static void handleBeforeInsert(List<Team_member__c> newList,  Map<Id, Team_member__c> oldMap) {
    }

    public static void handleAfterInsert(List<Team_member__c> newList,  Map<Id, Team_member__c> oldMap){
        TeamMember_utils.setRelatedAssignementsWeeklyCost(newList, oldMap);
    }

    public static void handleBeforeUpdate(List<Team_member__c> newList, Map<Id, Team_member__c> oldMap) {
    }

    public static void handleAfterUpdate(List<Team_member__c> newList,  Map<Id, Team_member__c> oldMap){
        TeamMember_utils.setRelatedAssignementsWeeklyCost(newList, oldMap);
    }
}