global class EmailManager {
    public List<EmailReceiver> receivers;
    public String emailTemplateId;
    public String emailBody;

    public EmailManager(List<EmailReceiver> receivers, String emailTemplateId, String emailBody) {
        this.receivers = receivers;
        this.emailTemplateId = emailTemplateId;
        this.emailBody = emailBody;
    }

    public void sendEmails() {
        for (EmailReceiver receiver : this.receivers) {
            this.sendEmail(receiver.emailAddress, receiver.targetObjectId, receiver.whatId);
        }
    }

    private void sendEmail(String receiverAddress, Id targetObjectId, Id whatId) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new List<String>{
                receiverAddress
        };
        message.setSenderDisplayName('Deloitte Support');
        message.setUseSignature(false);
        message.setBccSender(false);
        if (emailTemplateId != null) {
            message.setTemplateId(this.emailTemplateId);
            message.setTargetObjectId(targetObjectId);
            //message.plainTextBody = emailTemplate.Body;
            message.setWhatId(whatId);
        } else {
            message.plainTextBody = this.emailBody;
        }
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
                message
        };
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email has been sent successfully.');
        } else {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }
    }
}