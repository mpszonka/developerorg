/**
 * Created by mpszonka on 10.11.2019.
 */

public with sharing class TeamMember_utils {
    public static Boolean salaryFieldChanged(Team_member__c newValue, Team_member__c oldValue) {
        return newValue.Salary__c != oldValue.Salary__c;
    }

    public static void setRelatedAssignementsWeeklyCost(List<Team_member__c> newList, Map<Id, Team_member__c> oldMap) {
        List<Assignement__c> assignements = [SELECT weekly_cost__c, Team_member__c FROM Assignement__c ];
        for (Team_member__c newRecord : newList) {
            if (!Trigger.isInsert
                    && !TeamMember_utils.salaryFieldChanged(newRecord, oldMap.get(newRecord.Id))) {
                continue;
            }
            for (Assignement__c assignement : assignements) {
                if (assignement.Team_member__c == newRecord.Id) {
                    assignement.Weekly_cost__c = newRecord.Salary__c;
                }
            }
        }
        update assignements;
    }
}