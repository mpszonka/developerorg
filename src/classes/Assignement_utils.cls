/**
 * Created by mpszonka on 10.11.2019.
 */

public with sharing class Assignement_utils {

    public static Boolean teamMemberFieldChanged(Assignement__c newValue, Assignement__c oldValue) {
        return newValue.Team_member__c != oldValue.Team_member__c;
    }

    public static Boolean childProjectFieldChanged(Assignement__c newValue, Assignement__c oldValue) {
        return newValue.Child_project__c != oldValue.Child_project__c;
    }

    public static Boolean rateForGradeFieldChanged(Assignement__c newValue, Assignement__c oldValue) {
        return newValue.Rate_for_grade__c != oldValue.Rate_for_grade__c;
    }

    public static void setWeeklyCost(List<Assignement__c> newList, Map<Id, Assignement__c> oldMap) {
        Map<Id, Team_member__c> teamMembers = new Map<Id, Team_member__c>([SELECT Id, Salary__c FROM Team_member__c]);
        for (Assignement__c newRecord : newList) {
            if (!Trigger.isInsert
                    && !Assignement_utils.teamMemberFieldChanged(newRecord, oldMap.get(newRecord.Id))
                    && !Assignement_utils.childProjectFieldChanged(newRecord, oldMap.get(newRecord.Id))
                    && !Assignement_utils.rateForGradeFieldChanged(newRecord, oldMap.get(newRecord.Id))) {
                continue;
            }

            if (newRecord.Team_member__c != null) {
                newRecord.Weekly_cost__c = teamMembers.get(newRecord.Team_member__c).Salary__c;
            } else {
                newRecord.Weekly_cost__c = newRecord.Rate_for_grade__c;
            }
        }
    }
}