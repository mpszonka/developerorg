/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class EventMember_utils {
    public static void preventContactAssignmentIfEventIsNotExternal(List<Event_Member__c> newEventMembers) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Deloitte_Event__c == null) {
                continue;
            }
            Id newRelatedEventId = newEventMember.Deloitte_Event__c;
            List<Deloitte_s_Event__c> deloitteSEvents = [select Number_of_Participants__c, Type__c from Deloitte_s_Event__c where id = :newRelatedEventId];
            Deloitte_s_Event__c deloitteSEvent = deloitteSEvents[0];

            if (newEventMember.Contact__c != null && deloitteSEvent.Type__c != 'External') {
                newEventMember.addError('Cannot add account to external event');
            }
        }
    }

    public static void preventBothContactAndUserAssignment(List<Event_Member__c> newEventMembers) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Contact__c != null && newEventMember.User__c != null) {
                newEventMember.addError('Cannot add account and user, choose one');
            }
        }
    }

    public  static void prevenyNoCommentIfRejected(List<Event_Member__c> newEventMembers) {
        for (Event_Member__c newEventMember : newEventMembers) {
            if (newEventMember.Status__c == 'Rejected' && newEventMember.Comments__c == null) {
                newEventMember.addError('If status rejected, comments are needed');
            }
        }
    }
}