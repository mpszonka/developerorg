/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class DeloitteEventHandler {

    public static void handleBeforeInsert(List<SObject> newList) {

    }

    public static void handleBeforeUpdate(List<Deloitte_s_Event__c> deloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        DeloitteEvent_utilities.automaticallyApproveBasedOnCost(deloitteSEvents);
        DeloitteEvent_utilities.setCancellStatusIfRejected(deloitteSEvents);
        DeloitteEvent_utilities.preventMoreParticipantsThanSeats(deloitteSEvents);
        DeloitteEvent_utilities.calcPricePerParticipant(deloitteSEvents);
        DeloitteEvent_utilities.preventPendingApprovalStatus(deloitteSEvents);
    }
}