public with sharing class DeloitteEventListController {

    @AuraEnabled
    public static List<Deloitte_s_Event__c> getDeloitteSEventsWithApprovalStatusPending() {
        return [SELECT Id, Name, Approval_Status__c, Cost__c FROM Deloitte_s_Event__c WHERE Approval_Status__c = 'Pending'];
    }

    @AuraEnabled
    public static void setDeloitteEventApprovalStatusApproved(String deloitteEventId){
        Deloitte_s_Event__c deloitteSEvent = new Deloitte_s_Event__c(Id=deloitteEventId);
        deloitteSEvent.Approval_Status__c = 'Approved';
        update deloitteSEvent;
    }

    @AuraEnabled
    public static Boolean setDeloitteEventApprovalStatusRejected(String deloitteEventId, String rejectReason){
        Deloitte_s_Event__c deloitteSEvent = [SELECT Name, Status__c, Cost__c FROM Deloitte_s_Event__c WHERE Id=:deloitteEventId];
        deloitteSEvent.Approval_Status__c = 'Rejected';
        deloitteSEvent.Description__c = rejectReason;
        return Database.update(deloitteSEvent).isSuccess();
    }
}