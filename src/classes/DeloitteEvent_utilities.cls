/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class DeloitteEvent_utilities {

    private static Boolean numberOfParticipantsFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.Number_of_Participants__c != oldRecord.Number_of_Participants__c;
    }

    private static Boolean totalNumberOfSeatsFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.Total_Number_Of_Seats__c != oldRecord.Total_Number_Of_Seats__c;
    }

    private static Boolean costFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.Cost__c != oldRecord.Cost__c;
    }

    private static Boolean statusFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.Status__c != oldRecord.Status__c;
    }

    private static Boolean approvalStatusFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.Approval_Status__c != oldRecord.Approval_Status__c;
    }

    private static Boolean relatedEventFieldChanged(Event_Member__c newEventMember, Event_Member__c oldEventMember) {
        return newEventMember.Deloitte_Event__c != oldEventMember.Deloitte_Event__c;
    }

    private static Boolean externalIdFieldChanged(Deloitte_s_Event__c newRecord, Deloitte_s_Event__c oldRecord) {
        return newRecord.ExternalId__c != oldRecord.ExternalId__c;
    }

    public static void automaticallyApproveBasedOnCost(List<Deloitte_s_Event__c> newDeloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        for (Deloitte_s_Event__c newDeloitteSEvent : newDeloitteSEvents) {
            if (!Trigger.isInsert && !DeloitteEvent_utilities.costFieldChanged(newDeloitteSEvent, oldMap.get(newDeloitteSEvent.Id))) {
                continue;
            }

            if (newDeloitteSEvent.Cost__c < 100) {
                newDeloitteSEvent.Approval_Status__c = 'Approved';
            }
        }
    }

    public static void setCancelStatusIfRejected(List<Deloitte_s_Event__c> newDeloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        for (Deloitte_s_Event__c newDeloitteSEvent : newDeloitteSEvents) {
            if (!Trigger.isInsert && !DeloitteEvent_utilities.approvalStatusFieldChanged(newDeloitteSEvent, oldMap.get(newDeloitteSEvent.Id))) {
                continue;
            }

            if (newDeloitteSEvent.Approval_Status__c == 'Rejected') {
                newDeloitteSEvent.Status__c = 'Cancelled';
            }
        }
    }

    public static void preventMoreParticipantsThanSeats(List<Deloitte_s_Event__c> newDeloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        for (Deloitte_s_Event__c newDeloitteEvent : newDeloitteSEvents) {
            if (!Trigger.isInsert
                    && !DeloitteEvent_utilities.numberOfParticipantsFieldChanged(newDeloitteEvent, oldMap.get(newDeloitteEvent.Id))
                    && !DeloitteEvent_utilities.totalNumberOfSeatsFieldChanged(newDeloitteEvent, oldMap.get(newDeloitteEvent.Id))) {
                continue;
            }

            if (newDeloitteEvent.Number_of_Participants__c == null || newDeloitteEvent.Total_Number_Of_Seats__c == null) {
                continue;
            }

            if (newDeloitteEvent.Number_of_Participants__c > newDeloitteEvent.Total_Number_Of_Seats__c) {
                newDeloitteEvent.addError('Sorry, all places are booked!');
            }
        }
    }

    public static void calcPricePerParticipant(List<Deloitte_s_Event__c> newDeloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        for (Deloitte_s_Event__c newDeloitteEvent : newDeloitteSEvents) {
            if (!Trigger.isInsert
                    && !DeloitteEvent_utilities.numberOfParticipantsFieldChanged(newDeloitteEvent, oldMap.get(newDeloitteEvent.Id))
                    && !DeloitteEvent_utilities.costFieldChanged(newDeloitteEvent, oldMap.get(newDeloitteEvent.Id))) {
                continue;
            }
            if (newDeloitteEvent.Cost__c != null
                    && newDeloitteEvent.Number_of_Participants__c != null
                    && newDeloitteEvent.Number_of_Participants__c != 0) {
                newDeloitteEvent.Price_per_participant__c = newDeloitteEvent.Cost__c / newDeloitteEvent.Number_of_Participants__c;
            }
        }
    }

    public static void preventPendingApprovalStatus(List<Deloitte_s_Event__c> newDeloitteSEvents, Map<Id, Deloitte_s_Event__c> oldMap) {
        for (Deloitte_s_Event__c newDeloitteEvent : newDeloitteSEvents) {
            if (!Trigger.isInsert && !DeloitteEvent_utilities.statusFieldChanged(newDeloitteEvent, oldMap.get(newDeloitteEvent.Id))) {
                continue;
            }
            if (newDeloitteEvent.Status__c != 'New' && newDeloitteEvent.Status__c != 'Canceled' && newDeloitteEvent.Approval_Status__c == 'Pending') {
                newDeloitteEvent.addError('approval status cannot be pending if event status is not new or cancelled');
            }
        }
    }


    public static void calcNumberOfParticipants(List<Event_Member__c> newEventMembers, Map<Id, Event_Member__c> oldMap) {
        Set<Id> eventsIds = new Set<Id>();
        for (Event_Member__c newEventMember : newEventMembers) {
            eventsIds.add(newEventMember.Deloitte_Event__c);
        }
        if (!Trigger.isInsert) {
            for (Event_Member__c oldEventMember : oldMap.values()) {
                eventsIds.add(oldEventMember.Deloitte_Event__c);
            }
        }

        Map<Id, Deloitte_s_Event__c> deloitteSEvents = new Map<Id, Deloitte_s_Event__c>([SELECT Id, Number_of_Participants__c from Deloitte_s_Event__c WHERE Id IN :eventsIds]);

        for (Event_Member__c newEventMember : newEventMembers) {
            if (!Trigger.isInsert && !DeloitteEvent_utilities.relatedEventFieldChanged(newEventMember, oldMap.get(newEventMember.Id))) {
                continue;
            }
            Id newRelatedEventId = newEventMember.Deloitte_Event__c;
            if (newRelatedEventId == null) {
                continue;
            }

            Deloitte_s_Event__c newDeloitteSEvent = deloitteSEvents.get(newRelatedEventId);

            if (Trigger.isUpdate) {
                Id oldRelatedEventId = oldMap.get(newEventMember.Id).Deloitte_Event__c;
                if (oldRelatedEventId != null) {
                    newDeloitteSEvent.Number_of_Participants__c++;
                    deloitteSEvents.get(oldRelatedEventId).Number_of_Participants__c--;
                } else {
                    newDeloitteSEvent.Number_of_Participants__c++;
                }
                continue;
            }

            if (Trigger.isDelete) {
                newDeloitteSEvent.Number_of_Participants__c--;
                continue;
            }

            if (Trigger.isInsert) {
                newDeloitteSEvent.Number_of_Participants__c++;
                continue;
            }
        }
        update deloitteSEvents.values();
    }


    public static void setCostBasedOnExternalId(List<Deloitte_s_Event__c> newList, Map<Id, Deloitte_s_Event__c> oldMap) {
        Set<Id> changedEvents = new Set<Id>{
        };
        for (Deloitte_s_Event__c newDeloitteSEvent : newList) {
            if (Trigger.isUpdate) {
                if (DeloitteEvent_utilities.externalIdFieldChanged(newDeloitteSEvent, oldMap.get(newDeloitteSEvent.Id))) {
                    changedEvents.add(newDeloitteSEvent.Id);
                }
            } else {
                changedEvents.add(newDeloitteSEvent.Id);
            }
        }
        if (!changedEvents.isEmpty()) {
            DeloitteEvent_utilities.updateCostField(changedEvents);
        }
    }


    @future(callout = true)
    public static void updateCostField(Set<Id> deloitteEventsIds) {
        Map<String, Decimal> costs = DeloitteEvent_utilities.getExternalIdAndCostMap();
        List<Deloitte_s_Event__c> deloitteSEvents = [SELECT ExternalId__c, Cost__c, Id FROM Deloitte_s_Event__c WHERE Id IN :deloitteEventsIds];
        for (Deloitte_s_Event__c newDeloitteEvent : deloitteSEvents) {
            if (newDeloitteEvent.ExternalId__c != null) {
                newDeloitteEvent.Cost__c = costs.get(newDeloitteEvent.ExternalId__c);
            }
        }
        update deloitteSEvents;
    }

    public static Map<String, Decimal> getExternalIdAndCostMap() {
        List<String> externalIds = DeloitteEvent_utilities.getExternalIdPickListValues();
        Map<String, Double> costs = new Map<String, Double>();
        for (String externalId : externalIds) {
            costs.put(externalId, DeloitteEvent_fetchCost.getEventCost(externalId));
        }
        return costs;
    }

    public static List<String> getExternalIdPickListValues() {
        List<Schema.PicklistEntry> picklistEntries = Deloitte_s_Event__c.ExternalId__c.getDescribe().getPicklistValues();
        List<String> values = new List<String>();
        for (Schema.PicklistEntry picklistEntry : picklistEntries) {
            values.add(picklistEntry.getValue());
        }
        return values;
    }
}