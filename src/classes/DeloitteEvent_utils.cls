/**
 * Created by mpszonka on 30.10.2019.
 */

public with sharing class DeloitteEvent_utils {

    public static void automaticallyApproveBasedOnCost(List<Deloitte_s_Event__c> newDeloitteSEvents) {
        for (Deloitte_s_Event__c deloitteSEvent : newDeloitteSEvents) {
            if (deloitteSEvent.Cost__c < 100) {
                deloitteSEvent.Approval_Status__c = 'Approved';
            }
        }
    }

    public static void setCancellStatusIfRejected(List<Deloitte_s_Event__c> newDeloitteSEvents) {
        for (Deloitte_s_Event__c deloitteEvent : newDeloitteSEvents) {
            if (deloitteEvent.Approval_Status__c == 'Rejected') {
                deloitteEvent.Status__c = 'Cancelled';
            }
        }
    }

    public static void preventMoreParticipantsThanSeats(List<Deloitte_s_Event__c> newDeloitteSEvents) {
        for (Deloitte_s_Event__c deloitteEvent : newDeloitteSEvents) {
            if (deloitteEvent.Number_of_Participants__c == null || deloitteEvent.Total_Number_Of_Seats__c == null) {
                continue;
            }
            if (deloitteEvent.Number_of_Participants__c > deloitteEvent.Total_Number_Of_Seats__c) {
                deloitteEvent.addError('not enough amount of places!');
            }
        }
    }

    public static void calcPricePerParticipant(List<Deloitte_s_Event__c> newDeloitteSEvents) {
        for (Deloitte_s_Event__c deloitteEvent : newDeloitteSEvents) {
            if (deloitteEvent.Cost__c == null || deloitteEvent.Number_of_Participants__c == null) {
                continue;
            }
            deloitteEvent.Price_per_participant__c = deloitteEvent.Cost__c /= deloitteEvent.Number_of_Participants__c;
        }
    }

    public static void preventPendingApprovalStatus(List<Deloitte_s_Event__c> newDeloitteSEvents) {
        for (Deloitte_s_Event__c deloitteEvent : newDeloitteSEvents) {
            if (deloitteEvent.Status__c != 'New' && deloitteEvent.Status__c != 'Canceled' && deloitteEvent.Approval_Status__c == 'Pending') {
                deloitteEvent.addError('approval status cannot be pending if event status is not new or canceled');
            }
        }
    }

    public static void calcNumberOfParticipants(List<Event_Member__c> newEventMembers, Map<Id, Event_Member__c> oldMap) {
        Set<Id> eventsIds = new Set<Id>();
        for (Event_Member__c newEventMember : newEventMembers) {
            eventsIds.add(newEventMember.Deloitte_Event__c);
        }

        Map<Id, Deloitte_s_Event__c> deloitteSEvents = new Map<Id, Deloitte_s_Event__c>([SELECT Id, Number_of_Participants__c from Deloitte_s_Event__c WHERE Id IN :eventsIds]);

        for (Event_Member__c newEventMember : newEventMembers) {
            Id newRelatedEventId = newEventMember.Deloitte_Event__c;
            if (newRelatedEventId == null) {
                continue;
            }
            Deloitte_s_Event__c deloitteSEvent = deloitteSEvents.get(newRelatedEventId);

            if (Trigger.isUpdate) {
                Id oldRelatedEventId = oldMap.get(newEventMember.Id).Deloitte_Event__c;
                if (oldRelatedEventId != null && oldRelatedEventId != newRelatedEventId) {
                    deloitteSEvent.Number_of_Participants__c--;
                } else {
                    deloitteSEvent.Number_of_Participants__c++;
                }
                update deloitteSEvent;
                continue;
            }

            if (Trigger.isDelete) {
                deloitteSEvent.Number_of_Participants__c--;
                update deloitteSEvent;
                continue;
            }

            if (Trigger.isInsert) {
                deloitteSEvent.Number_of_Participants__c++;
                update deloitteSEvent;
                continue;
            }
        }
    }
}