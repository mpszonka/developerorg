/**
 * Created by mpszonka on 06.11.2019.
 */

({
    changeApprovalStatus: function (cmp, deloitteEventId) {
        var action = cmp.get("c.setDeloitteEventApprovalStatusApproved");
        action.setParams({deloitteEventId});
        action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === 'SUCCESS') {
                alert('success');
                this.fetchData(cmp);
           } else {
               alert('not success');
           }
        })
        $A.enqueueAction(action);
    },

    changeApprovalStatusToRejected: function(component, deloitteEventId, rejectReason) {
          var action = component.get("c.setDeloitteEventApprovalStatusRejected");
          action.setParams({deloitteEventId,
                            rejectReason});
          action.setCallback(this, function(response) {
             var state = response.getState();
             if (state === 'SUCCESS') {
                alert('success');
                //toastEvent
                this.fetchData(component);
             } else {
                 alert('not success');
             }
          })
          $A.enqueueAction(action);
    },

    fetchData: function(cmp) {
        const actions = [
           { label: 'Accept', name: 'accept' },
           { label: 'Reject', name: 'reject' }
        ];
        cmp.set('v.columns', [
                   { label: 'Event name', fieldName: 'Name', type: 'text'},
                   { label: 'Cost', fieldName: 'Cost__c', type: 'currency'},
                   { label: 'Approval status', fieldName: 'Approval_Status__c', type: 'text'},
                   { type: 'action',
                     typeAttributes: { rowActions: actions },
                   }]
        );
        var action = cmp.get("c.getDeloitteSEventsWithApprovalStatusPending");
        action.setCallback(this, function(response) {
           var state = response.getState();
           if (state === "SUCCESS") {
               cmp.set("v.data", response.getReturnValue());
           } else {
               alert('cannot fetch deloitte events list');
           }
        })
        $A.enqueueAction(action);
    },

   openModal: function(component) {
      component.set("v.isOpen", true);
   },
});