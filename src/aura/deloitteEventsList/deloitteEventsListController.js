({
    init: function (cmp, event, helper) {
        helper.fetchData(cmp, event, helper);
    },

    handleEvent: function(cmp, event, helper) {
        console.log('event handled');
        helper.fetchData(cmp, event, helper);
        console.log('data fetched');
    },

    handleRowAction: function (cmp, event, helper)
    {
        var action = event.getParam('action');
        var row = event.getParam('row');
        confirm('Do you want to change approval status to ', action.name, '?');
        switch (action.name) {
            case 'accept':
                  helper.changeApprovalStatus(cmp, row.Id, 'Approved');
                break;
            case 'reject':
                cmp.set("v.currentRecordId", row.Id);
                helper.openModal(cmp);
                break;
        }
    },

    closeModal: function(component, event, helper) {
       component.set("v.isOpen", false);
    },

    saveAndClose: function(component, event, helper) {
       var recordId = component.get("v.currentRecordId");
       var rejectReason = component.find("rejectReason").get("v.value");
       helper.changeApprovalStatusToRejected(component, recordId, rejectReason);
       component.set("v.isOpen", false);
    }
});
