/**
 * Created by mpszonka on 06.11.2019.
 */

({
    init: function(cmp, event, helper) {
        const userId = $A.get("$SObjectType.CurrentUser.Id");
        cmp.set("v.userId", userId);
    },

    handleIncrementScoring: function(cmp, event, helper) {
        const oldVoteValue = cmp.get("v.voteValue");
        if (oldVoteValue < 1)
            cmp.set("v.voteValue", oldVoteValue + 1);
    },

    handleDecrementScoring: function(cmp, event, helper) {
        const oldVoteValue = cmp.get("v.voteValue");
        if (oldVoteValue > -1)
            cmp.set("v.voteValue", oldVoteValue - 1);
    },

    handleSuccess: function(){
        alert('Voted!');
    },
});