/**
 * Created by mpszonka on 06.11.2019.
 */

({
    init: function(cmp, event, helper) {
        //console.log(component.find("recordId").get("v.value"));
        //cmp.set('v.recordId', null);
    },

    handleSavingError: function(cmp, event, helper) {
        event.preventDefault();
        const toastEvent = $A.get("e.force:showToast");
        helper.showErrorToast(event.detail.detail);
    },

    handleSavingSuccess : function(cmp, event, helper) {
        $A.get("e.c:newDeloitteEventCreated").fire();
        helper.showSuccessToast("The record has been added successfully.");
        cmp.set('v.recordId', null);
    }
});