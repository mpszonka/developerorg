/**
 * Created by mpszonka on 10.11.2019.
 */

trigger AssignementTrigger on Assignement__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (Trigger.isBefore && Trigger.isInsert) {
        Assignement_triggerHandler.handleBeforeInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isInsert) {
    }
    else if (Trigger.isBefore && Trigger.isUpdate) {
        Assignement_triggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isUpdate) {
    }
    else if (Trigger.isBefore && Trigger.isDelete) {
    }
}