/**
 * Created by mpszonka on 30.10.2019.
 */

trigger EventMember_trigger on Event_Member__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (Trigger.isBefore && Trigger.isInsert) {
        EventMember_triggerHandler.handleBeforeInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isInsert) {
        EventMember_triggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isBefore && Trigger.isUpdate) {
        EventMember_triggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isUpdate) {
        EventMember_triggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isBefore && Trigger.isDelete) {
        EventMember_triggerHandler.handleAfterDelete(Trigger.new, Trigger.oldMap);
    }
}