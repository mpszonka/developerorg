/**
 * Created by mpszonka on 10.11.2019.
 */

trigger TeamMemberTrigger on Team_member__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    if (Trigger.isBefore && Trigger.isInsert) {
        TeamMember_triggerHandler.handleBeforeInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isInsert) {
        TeamMember_triggerHandler.handleAfterInsert(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isBefore && Trigger.isUpdate) {
        TeamMember_triggerHandler.handleBeforeUpdate(Trigger.new, Trigger.oldMap);
    }
    else if (Trigger.isAfter && Trigger.isUpdate) {
        TeamMember_triggerHandler.handleAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}