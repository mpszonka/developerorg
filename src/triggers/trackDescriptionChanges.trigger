/**
 * Created by mpszonka on 29.10.2019.
 */

trigger trackDescriptionChanges on test_object__c (before update) {
    for (test_object__c testObjectRecord : Trigger.new) {
        if (testObjectRecord.description__c != Trigger.oldMap.get(testObjectRecord.id).description__c) {
            if (testObjectRecord.Count_of_changes__c < 5) {
                testObjectRecord.Count_of_changes__c++;
            } else {
                testObjectRecord.addError('Cannot change more than 5 times!');
            }
        } else {
            testObjectRecord.addError('Description was not changed!');
        }
    }
}